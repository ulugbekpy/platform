from django.urls import path
from .views import home, emp_detail, save_emp


urlpatterns = [
    path("", home, name="home"),
    path("employee/<int:pk>/", emp_detail, name="employee-detail"),
    path("employee/new/", save_emp, name="save-emp"),
]
