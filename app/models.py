from django.db import models


class Employee(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.EmailField(max_length=100, unique=True)
    phone_number = models.CharField(max_length=12, blank=True)
    image = models.ImageField(upload_to="images", null=True, blank=True)

    def __str__(self) -> str:
        return self.first_name + " " + self.last_name
