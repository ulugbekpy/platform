from django import forms
from .models import Employee


class SaveForm(forms.ModelForm):
    class Meta:
        model = Employee
        fields = "__all__"
