from django.shortcuts import render, get_object_or_404, redirect
from .models import Employee
from .forms import SaveForm


# Create your views here.
def home(request):
    employees = Employee.objects.all()

    context = {"employees": employees}
    return render(request, "home.html", context=context)


def emp_detail(request, pk):
    employee = get_object_or_404(Employee, pk=pk)
    return render(request, "empdet.html", {"employee": employee})


def save_emp(request):
    context = {}

    form = SaveForm()

    if request.method == "POST":
        form = SaveForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("home")

    context["form"] = form
    return render(request, "emp_create.html", context)
